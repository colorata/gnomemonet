'use strict';

const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;
const Gtk = imports.gi.Gtk;
const Config = imports.misc.config;
const [major] = Config.PACKAGE_VERSION.split('.');
const shellVersion = Number.parseInt(major);
const gtkVersion = Gtk.get_major_version();
const Gio = imports.gi.Gio;
const Clutter = imports.gi.Clutter;
const GdkPixbuf = imports.gi.GdkPixbuf; 
const GdkPixbufVersion = Number.parseInt(GdkPixbuf.PIXBUF_VERSION);

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const MarvinJ = Me.imports.MarvinJ;
const Converter = new MarvinJ.MarvinColorModelConverter();

class MonetLib {

	/**
	 * function to read file.
	 * @param {string} filename **absolute** path to file like **"/path/to/file"**.
	 * @returns content of file
	 * @example
	 * // /home/user/file.txt: "abcd"
	 * // expected output: "abcd"
	 * let output = new MonetLib().readFile("/home/user/file.txt");
	 */
	readFile(filename) {
		return String(GLib.file_get_contents(filename)[1]);
	}

	/**
	 * function to write to file.
	 * @param {string} filename **absolute** path to file like **"/path/to/file"**.
	 * @param {string} content what to put to file.
	 * @example
	 * // (before state) /home/user/file.txt: "abcd"
	 * // (after state) /home/user/file.txt: "zyx"
	 * let output = new MonetLib().writeFile("/home/user/file.txt", "zyx");
	 */
	writeFile(filename, content) {
		GLib.file_set_contents(filename, content);
	}

	/**
	 * function to get luminance from rgb color.
	 * @param {number} r **red** value of color between 0 and 255.
	 * @param {number} g **green** value of color between 0 and 255.
	 * @param {number} b **blue** value of color between 0 and 255.
	 * @returns {number} luminance of this color between 0 and 100.
	 * @example
	 * // expected output: 100
	 * let output = new MonetLib().luminanceFromRGB(255, 255, 255);
	 */
	luminanceFromRGB(r, g, b) {
		return (0.2126 * r / 255 + 0.7152 * g / 255 + 0.0722 * b / 255) * 100;
	}

	/**
	 * function to get all occurrences in string.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/3410557/15061682).
	 * @param {String} searchStr what string to search.
	 * @param {String} str where to search.
	 * @param {boolean} caseSensitive is search case sensitive.
	 * @returns {Array.<number>} list of indexes of occurrences.
	 * @example
	 * //expected output: [2, 25, 27, 33]
	 * let output = new MonetLib().getIndicesOf("le", "I learned to play the Ukulele in Lebanon.", false);
	 */
	getIndicesOf(searchStr, str, caseSensitive) {
		var searchStrLen = searchStr.length;
		if (searchStrLen == 0) {
			return [];
		}
		var startIndex = 0, index, indices = [];
		if (!caseSensitive) {
			str = str.toLowerCase();
			searchStr = searchStr.toLowerCase();
		}
		while ((index = str.indexOf(searchStr, startIndex)) > -1) {
			indices.push(index);
			startIndex = index + searchStrLen;
		}
		return indices;
	}

	/**
	 * function to replace chars after **count** of sougth characters.
	 * @param {string} filename **absolute** path to file like **"/path/to/file"**.
	 * @param {number} count count of sougth characters.
	 * @param {string} to string to what to replace.
	 * @param {string} character what character to search.
	 * @example
	 * // (before state) /home/user/file.txt: "$1000 $10 $ffffff"
	 * // (after state) /home/user/file.txt: "$1000 $10 $c3c3c3"
	 * let output = new MonetLib().replaceFile("/home/user/file.txt", 3, "c3c3c3", "$");
	 */
	replaceFile(filename, count, to, character = "#") {
		let content = this.readFile(filename);
		let indicesList = this.getIndicesOf(character, content, false);
		content = content.substring(0, indicesList[count - 1] + 1) + to + content.substring(indicesList[count - 1] + 7, content.length);
		this.writeFile(filename, content);
	}

	/**
	 * function for **sync** execution in shell.
	 * 
	 * ***Warning***: this function can *freeze* your ui.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/q/33451496/15061682).
	 * @param {string} command command in shell to execute.
	 * 
	 * **Note**: use **full paths** to executables:
	 *  - ✘ *"ls"*
	 *  - ✔ *"/bin/ls"*
	 * @returns {string} result of execution command.
	 * @example
	 * // expected output: "/home/user".
	 * let output = new MonetLib().runTerminal("/bin/pwd");
	 */
	runTerminal(command) {
		const [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(null, command.split(' '), null, 0, null);
		let out_reader = new Gio.DataInputStream({
			base_stream: new Gio.UnixInputStream({fd: out_fd})
		});
		var out = out_reader.read_upto("", 0, null)[0];
		log("Out is " + out);
		return String(out);
	}

	/**
	 * Callback for terminal Async output
	 * @callback terminalAsyncCallback function with result(String) parameter that will be executed **after** full command execution.
	 * @param {string} output result of execution command
	 */

	/**
	 * function for **async** execution in shell.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/q/33451496/15061682).
	 * @param {string} command command in shell to execute.
	 * 
	 * **Note**: use **full paths** to executables:
	 *  - ✘ *"ls"*
	 *  - ✔ *"/bin/ls"*
	 * @param {terminalAsyncCallback} callback function with result(String) parameter that will be executed **after** full command execution.
	 * @example
	 * // expected: text in gtkLabel: "/home/user".
	 * let callback = function(r) {
	 * 	gtkLabel.set_label(r);
	 * }
	 * new MonetLib().runTerminalAsync("/bin/pwd", callback);
	 */
	runTerminalAsync(command, callback = function(result) { }) {
		const [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(null, command.split(' '), null, 0, null);
		const out_reader = new Gio.DataInputStream({
			base_stream: new Gio.UnixInputStream({fd: out_fd})
		});
		let final = "";
		let isUse = false;
		function _SocketRead(_source_object, res){
			const [out, length] = out_reader.read_upto_finish(res);
			if (length > 0) {
				log("out: " + out);
				final = final + out + "\n";
				log("length: " + length);
				out_reader.read_upto_async("", 0, 0, null, _SocketRead);
			} {
				callback(final);
			}
		}
		out_reader.read_upto_async("", 0, 0, null, _SocketRead);
	}

	/**
	 * function to get current user's wallpaper.
	 * @returns {string} **absolute** path to wallpaper like **"/path/to/wallpaper"**.
	 * @example
	 * // expected output: "'file:///usr/share/backgrounds/fedora-workstation/frog-prince.jpg'"
	 * let output = new MonetLib().getWallpaper();
	 */
	getWallpaper() {
		return this.runTerminal("/bin/gsettings get org.gnome.desktop.background picture-uri");
	}

	/**
	 * function to get path of executable .js file.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/14078345/15061682).
	 * @returns {string} **absolute** path to executable like **"/path/to/parent"**.
	 * @example
	 * // expected output: "/home/user/.local/share/gnome-shell/extensions/colorata@gnomemonet"
	 * let output = new MonetLib().getCurrentFile();
	 */
	getCurrentFile() {
		let stack = (new Error()).stack;
		let stackLine = stack.split('\n')[1];
		if (!stackLine)
			throw new Error('Could not find current file');
		
		let match = new RegExp('@(.+):\\d+').exec(stackLine);
		if (!match)
			throw new Error('Could not find current file');
	
		let path = match[1];
		let file = Gio.File.new_for_path(path);
		return file.get_parent().get_path();
	}

	/**
	 * function that converts **hex(decimal or hexagonal)** value to normal hex string.
	 * 
	 * source - source - [Stackoverflow](https://stackoverflow.com/a/12579936/15061682).
	 * @param {number} color hex value like **0xFFFFFFFF** or decimal value like **-1**.
	 * @returns {string} string value in normal hex like **"FFFFFF"**.
	 * @example
	 * // expected output: "FFFFFF"
	 * let output = new MonetLib().argbToRGB(-1);
	 */
	argb2RGB(color) {
		return ('000000' + (color & 0xFFFFFF).toString(16)).slice(-6);
	}

	/**
	 * function that predicts **main** color of image.
	 * 
	 * @param {string} imagePath path to image **(without "file://" prefix)**.
	 * @param {number} accuracy accuracy of color predicting **(lower value - higher accuracy)**.
	 * @returns {number} value between 0 and 360 - **Hue** value in 'HSV' color system.
	 * @example
	 * // expected output: 160
	 * let output = new MonetLib().mainPredict("/usr/share/backgrounds/fedora-workstation/frog-prince.jpg", 10);
	 */
	mainPredict(imagePath = this.getWallpaper(), accuracy = 20) {
		let minusValue = 3;
		imagePath = imagePath.substring(8, imagePath.length - 2);
		if (imagePath.substring(imagePath.length - 3, imagePath.length) === "png") minusValue = 4;
		log(imagePath.substring(imagePath.length - 3, imagePath.length));
		let executablePath = this.getCurrentFile();
		imagePath.replaceAll("%20", " ");
		let image = GdkPixbuf.Pixbuf.new_from_file(imagePath);
		let imageWidth = image.get_width();
		let imageHeight = image.get_height();
		let imagePixels = image.get_pixels();
		if (GdkPixbufVersion >= 2.26) imagePixels = image.get_pixels()[0];
		log(imagePixels[0] + " " + imagePixels[1] + " " + imagePixels[2] + " " + imagePixels[3] + " " + imagePixels[4] + " " + imagePixels[5]);
		log(imagePixels[6] + " " + imagePixels[7] + " " + imagePixels[8] + " " + imagePixels[9] + " " + imagePixels[10] + " " + imagePixels[11]);
		let rFinal = 0;
		let gFinal = 0;
		let bFinal = 0;
		let hFinal = new Array(360).fill(0);
		for (let i = 0; i < imagePixels.length; i += minusValue * accuracy) {
			rFinal += imagePixels[i];
			gFinal += imagePixels[i + 1];
			bFinal += imagePixels[i + 2];
			let hIndex = this.rgb2hsv(imagePixels[i], imagePixels[i + 1], imagePixels[i + 2])[0];
			hFinal[Math.round(hIndex)] += 1;
		}
		rFinal = rFinal / (imageWidth * imageHeight);
		gFinal = gFinal / (imageWidth * imageHeight);
		bFinal = bFinal / (imageWidth * imageHeight);
		let maxOccurence1 = 0;
		let maxOccurence2 = 0;
		let indexOccurence1 = 0;
		let indexOccurence2 = 0;
		for (let i = 0; i < 360; i++) {
			if (maxOccurence1 < hFinal[i]) {
				maxOccurence1 = hFinal[i];
				indexOccurence1 = i;
			}
		}

		return indexOccurence1;
	}

	/**
	 * function that compiles scss styles with predicted color variants to gtk.css files with sass.
	 * 
	 * **Note**: now outputs located in Compiled and Compiled-dark folder.
	 */
	mainCompile() {
		let executablePath = this.getCurrentFile();
		let h = this.mainPredict();
		let rgbForeground = this.hsv2rgb(h, 70, 80);
		let rgbBackgroundDark = this.hsv2rgb(h, 33, 20);
		let rgbBackgroundLight = this.hsv2rgb(h, 5, 90);
		let accentHex = this.rgb2hex(rgbForeground[0], rgbForeground[1], rgbForeground[2]);
		let darkHex = this.rgb2hex(rgbBackgroundDark[0], rgbBackgroundDark[1], rgbBackgroundDark[2]);
		let lightHex = this.rgb2hex(rgbBackgroundLight[0], rgbBackgroundLight[1], rgbBackgroundLight[2]);
		log(rgbForeground);
		log(rgbBackgroundDark);
		log(rgbBackgroundLight);
		log(h);
		log(accentHex);
		
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.general.accent, accentHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.general.accent, accentHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.general.accent, accentHex);

		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.light.base, lightHex);
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.dark.base, darkHex);
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.light.background, lightHex);
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.dark.background, darkHex);
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.light.foreground, darkHex);
		this.replaceFile(executablePath + this.objects.gtk3.path, this.objects.colors.dark.foreground, lightHex);

		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.light.base, lightHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.dark.base, darkHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.light.background, lightHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.dark.background, darkHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.light.foreground, darkHex);
		this.replaceFile(executablePath + this.objects.gtk4.path, this.objects.colors.dark.foreground, lightHex);

		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.light.base, lightHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.dark.base, darkHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.light.background, lightHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.dark.background, darkHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.light.foreground, darkHex);
		this.replaceFile(executablePath + this.objects.gnome.path, this.objects.colors.dark.foreground, lightHex);

		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gtk3.light.execPath + " " + executablePath + this.objects.gtk3.light.buildedPath);
		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gtk3.dark.execPath + " " + executablePath + this.objects.gtk3.dark.buildedPath);
		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gtk4.light.execPath + " " + executablePath + this.objects.gtk4.light.buildedPath);
		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gtk4.dark.execPath + " " + executablePath + this.objects.gtk4.dark.buildedPath);
		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gnome.light.execPath + " " + executablePath + this.objects.gnome.light.buildedPath);
		this.runTerminalAsync(executablePath + "/sass/sass " + executablePath + this.objects.gnome.dark.execPath + " " + executablePath + this.objects.gnome.dark.buildedPath);
	}
	
	/**
	 * function that converts r, g, b color to **decimal**.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/4801446/15061682).
	 * @param {number} r **red** value of color between 0 and 255.
	 * @param {number} g **green** value of color between 0 and 255.
	 * @param {number} b **blue** value of color between 0 and 255.
	 * @returns {number} decimal value of this color like **-1** for **255, 255, 255**.
	 * @example
	 * // expected output: -1
	 * let output = new MonetLib().rgbToInt(255, 255, 255);
	 */
	rgb2int(r, g, b) {
		return 0xffff * r + 0xff * g + b;
	}

	/**
	 * function that converts color from **rgb** space to **hsv** space.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/54024653/15061682).
	 * @param {number} r **red** value of color between 0 and 255.
	 * @param {number} g **greeen** value of color between 0 and 255.
	 * @param {number} b **blue** value of color between 0 and 255.
	 * @returns {Array.<number>} list with HSV values like [120, 100, 100] for 0, 255, 0.
	 * @example
	 * // expected output: [120, 100, 100]
	 * let output = new MonetLib().rgb2hsv(0, 255, 0);
	 */
	rgb2hsv(r,g,b) {
		let v=Math.max(r,g,b), n=v-Math.min(r,g,b);
		let h= n && ((v==r) ? (g-b)/n : ((v==g) ? 2+(b-r)/n : 4+(r-g)/n)); 
		return [60*(h<0?h+6:h), v&&n/v, v];
	}

	/**
	 * function that converts color from **hsv** space to **rgb** space.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/54024653/15061682)
	 * @param {number} h **hue** value of color between 0 and 360.
	 * @param {number} s **saturation** value of color between 0 and 100.
	 * @param {number} v **value** value of color between 0 and 100.
	 * @returns {Array.<number>} color in **rgb** space.
	 * @example
	 * // expected output: [0, 255, 0]
	 * let output = new MonetLib().hsv2rgb(120, 100, 100);
	 */
	hsv2rgb(h,s,v) {
		s /= 100;
		v /= 100;
		let f= (n,k=(n+h/60)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);
		return [Math.round(f(5) * 255), Math.round(f(3) * 255), Math.round(f(1) * 255)];
	}

	/**
	 * Converts **rgb** color space to **hex** color space.
	 * 
	 * Source - [Stackoverflow](https://stackoverflow.com/a/5624139/15061682)
	 * @param {number} r **red** value of color between 0 and 255.
	 * @param {number} g **green** value of color between 0 and 255.
	 * @param {number} b **blue** value of color between 0 and 255.
	 * @returns {string} **hex** value of color
	 * @example
	 * // expected output: "00FF00".
	 * new MonetLib().rgb2Hex(0, 255, 0);
	 */
	rgb2hex(r, g, b) {
		return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	}

	/**
	 * Objects for theming like paths to scss files, location of colors in files.
	 */
	objects = {
		colors: {
			light: {
				base: 1, // #ffffff
				background: 2, // #f6f5f4
				foreground: 3 // #2e3436
			},
			dark: {
				base: 4, // #241f31
				background: 5, // #3d3846
				foreground: 6 // #eeeeec
			},
			general: {
				warning: 7, // #f57900
				error: 8, // #cc0000
				success: 9, // #33d17a
				destructive: 10, // #e01b24
				accent: 11 // #3584e4
			}
		},
		gtk4: {
			path: "/Defaults/gtk-4.0-new/_colors.scss",
			light: {
				execPath: "/Defaults/gtk-4.0-new/Default-light.scss",
				buildedPath: "/Compiled/gtk-4.0/gtk.css",
			},
			dark: {
				execPath: "/Defaults/gtk-4.0-new/Default-dark.scss",
				buildedPath: "/Compiled-dark/gtk-4.0/gtk.css",
			}
		},
		gtk3: {
			path: "/Defaults/gtk-3.0-new/_colors.scss",
			light: {
				execPath: "/Defaults/gtk-3.0-new/gtk-contained.scss",
				buildedPath: "/Compiled/gtk-3.0/gtk.css",
			},
			dark: {
				execPath: "/Defaults/gtk-3.0-new/gtk-contained-dark.scss",
				buildedPath: "/Compiled-dark/gtk-3.0/gtk.css",
			}
		},
		gnome: {
			path: "/Defaults/gnome-shell/gnome-shell-sass/_colors.scss",
			light: {
				execPath: "/Defaults/gnome-shell/gnome-shell-light.scss",
				buildedPath: "/Compiled/gnome-shell/gnome-shell.css",
			},
			dark: {
				execPath: "/Defaults/gnome-shell/gnome-shell-dark.scss",
				buildedPath: "/Compiled-dark/gnome-shell/gnome-shell.css",
			}
		}
	}
}
