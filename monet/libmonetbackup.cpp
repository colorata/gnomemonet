#include <iostream>
#include <string>
#include <cmath>
#include <array>
#include <stdexcept>
#include <cstdio>
#include <memory>
#include <algorithm>
#include <sstream>
#include <vector>
using namespace std;

vector<string> split (const string &s, char delim) {
    vector<string> result;
    stringstream ss (s);
    string item;

    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}

string exec(const char* cmd) {
    array<char, 128> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

string replace(string str, const string& from, const string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}

string getPixel(const string path, const int x, const int y) {
	return exec(("convert " + string("\"") + path + string("\"") + " -format \"#%[hex:u.p{" + to_string(x) + ", " + to_string(y) + string("}]\" info:-")).c_str());
}

vector<int> getImageSize(const string path) {
	vector<int> result;
	vector<string> res = split(exec(("identify -format '%w %h' " + string("\"") + path + string("\"")).c_str()), ' ');
	for (int i = 0; i < res.size(); i++) {
		result.push_back(atoi(res[i].c_str()));
	}
	return result;
}
int main(){
    string picture = exec("gsettings get org.gnome.desktop.background picture-uri");
	picture.erase(0, 8);
	picture.erase(picture.length() - 2, picture.length());
	picture = replace(picture, "%20", " ");
    //cout << getPixel(picture, 75, 20);
    cout << getImageSize(picture)[0];
    vector<int> size = getImageSize(picture);
    vector<vector<string>> image;
    for (int i = 0; i < size[0]; i++) {
		vector<string> buffer;
    	for (int j = 0; j < size[1]; j++) {
    		buffer.push_back(getPixel(picture, i, j));
    		cout << j << endl;
    	}
    	cout << i << endl;
    	image.push_back(buffer);
    }
    cout << "Done";
	return 0;
}
