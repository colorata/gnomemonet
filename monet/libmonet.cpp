#include <iostream>
#include <string>
#include <cmath>
#include <array>
#include <stdexcept>
#include <cstdio>
#include <memory>
#include <algorithm>
#include <sstream>
#include <vector>
#include <Magick++.h>
#include "monetUtils.cpp"
#define QuantumRange ((Quantum) 65535)
using namespace std;
using namespace Magick;

PixelPacket *pixels;
int range;
vector<string> split (const string &s, char delim) {
    vector<string> result;
    stringstream ss (s);
    string item;

    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}

vector<int> toHSV(float fR, float fG, float fB) {
	float fH = 0;
	float fS = 0;
	float fV = 0;
	float fCMax = max(max(fR, fG), fB);
	float fCMin = min(min(fR, fG), fB);
	float fDelta = fCMax - fCMin;
  
	if(fDelta > 0) {
		if(fCMax == fR) {
			fH = 60 * (fmod(((fG - fB) / fDelta), 6));
		} else if(fCMax == fG) {
			fH = 60 * (((fB - fR) / fDelta) + 2);
		} else if(fCMax == fB) {
			fH = 60 * (((fR - fG) / fDelta) + 4);
		}
    
	if(fCMax > 0) {
		fS = fDelta / fCMax;
	} else {
		fS = 0;
	}
    fV = fCMax;
	} else {
		fH = 0;
		fS = 0;
		fV = fCMax;
	}  
	if(fH < 0) {
		fH = 360 + fH;
	}
	vector<int> result;
	result.push_back((int)fH);
	result.push_back((int)(fS * 100));
	result.push_back((int)fV);
	return result;
}

string replace(string str, const string& from, const string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}

vector<int> getPixel(Image image, const int x, const int y) {
			vector<int> result;
			result.push_back(pixels[image.columns() * y + x].red * 255 / QuantumRange);
			result.push_back(pixels[image.columns() * y + x].green * 255 / QuantumRange);
			result.push_back(pixels[image.columns() * y + x].blue * 255 / QuantumRange);
			return result;
		}

vector<float> toHSV1(int r, int g, int b) {
	vector<float> result;
	float localR = (float)r / 255;
	float localG = (float)g / 255;
	float localB = (float)b / 255;
	float maxof = max(localR, max(localG, localB));
	float minof = min(localR, min(localG, localB));
	float dif = maxof - minof;
	float h = 0;
	float s = 0;
	float v = 0;
	v = maxof * 100;
	if (minof == maxof) h = 0;
	if (maxof = localR && localG >= localB) {
		h = 60 * (localG - localB) / dif;
	} else if (maxof == localR && localG < localB) {
		h = 60 * (localG - localB) / dif + 360;
	} else if (maxof == localG) {
		h = 60 * (localB - localR) / dif + 120;
	} else if (maxof == localB) {
		h = 60 * (localR - localG) / dif + 240;
	}
	if (maxof == 0.00) s = 0;
	else s = (1 - minof / maxof) * 100;
	result.push_back(h);
	result.push_back(s);
	result.push_back(v);
	return result;
	
}

vector<int> toRGB(float H, float S,float V){
    
    float s = S/100;
    float v = V/100;
    float C = s*v;
    float X = C*(1-abs(fmod(H/60.0, 2)-1));
    float m = v-C;
    float r,g,b;
    if(H >= 0 && H < 60){
        r = C,g = X,b = 0;
    }
    else if(H >= 60 && H < 120){
        r = X,g = C,b = 0;
    }
    else if(H >= 120 && H < 180){
        r = 0,g = C,b = X;
    }
    else if(H >= 180 && H < 240){
        r = 0,g = X,b = C;
    }
    else if(H >= 240 && H < 300){
        r = X,g = 0,b = C;
    }
    else{
        r = C,g = 0,b = X;
    }
    int R = (r+m)*255;
    int G = (g+m)*255;
    int B = (b+m)*255;
    vector<int> result;
    result.push_back(R);
    result.push_back(G);
    result.push_back(B);
    return result;
}
int main(int argc,char **argv){
	InitializeMagick(*argv);
    string picture = exec("gsettings get org.gnome.desktop.background picture-uri");
	picture.erase(0, 8);
	picture.erase(picture.length() - 2, picture.length());
	picture = replace(picture, "%20", " ");
	Image image(picture);
	range = pow(2, image.modulusDepth());
	const int width = image.columns();
	const int height = image.rows();
	pixels = image.getPixels(0, 0, width, height);
	long long rFinal = 0;
	long long gFinal = 0;
	long long bFinal = 0;
	vector <vector <int> > r(height + 1,vector <int> (width + 1));
	vector <vector <int> > g(height + 1,vector <int> (width + 1));
	vector <vector <int> > b(height + 1,vector <int> (width + 1));
	long long vectorH[360] = { 0 };
	for (int i=1; i < height; i++) {
		for (int j=1; j < width; j++) {
			vector <int> buffer = getPixel(image, j, i);
			if (buffer[0] == buffer[1] && buffer[0] == buffer[2]) continue;
			r[i][j]=buffer[0];
			g[i][j]=buffer[1];
			b[i][j]=buffer[2];
			vectorH[toHSV((float)buffer[0], (float)buffer[1], (float)buffer[2])[0]] += 1;
			rFinal = rFinal + buffer[0];
			gFinal = gFinal + buffer[1];
			bFinal = bFinal + buffer[2];
		}
	}
	rFinal = rFinal/(height * width);
	gFinal = gFinal/(height * width);
	bFinal = bFinal/(height * width);
	vector<int> hsv = toHSV((float)rFinal, (float)gFinal, (float)bFinal);
	vector<int> rgb = toRGB((float)hsv[0], (float)70, (float)80);
	long long max1 = 0;
	long long max2 = 0;
	int index1 = 0;
	int index2 = 0;
	for (int i = 0; i < 360; i++) {
		if (max1 < vectorH[i]) {
			max1 = vectorH[i];
			index1 = i;
		}
	}
	for (int i = 0; i < 360; i++) {
		if (i < index1 + 30 && i > index1 - 30) continue;
		if (max2 < vectorH[i]) {
			max2 = vectorH[i];
			index2 = i;
		}
	}
	cout << "H1 = " << index1 << " H2 = " << index2 << endl;
	cout << "H1 = " << max1 << " H2 = " << max2 << endl;
	cout << "R = " << rFinal << " G = " << gFinal << " B = " << bFinal << endl;
	cout << "H = " << hsv[0] << " S = " << hsv[1] << " V = " << hsv[2] << endl;
	cout << "R = " << rgb[0] << " G = " << rgb[1] << " B = " << rgb[2] << endl;
	vector<int> rgbFinal = toRGB((float)index1, (float)70, (float)80);
	string hexFinal = toHex(rgb[0], rgb[1], rgb[2]);
	vector<int> backgroundDarkFinal = toRGB((float)index1, (float)33, (float)13);
	string backgroundDarkHex = toHex(backgroundDarkFinal[0], backgroundDarkFinal[1], backgroundDarkFinal[2]);
	vector<int> backgroundLightFinal = toRGB((float)index1, (float)5, (float)90);
	string backgroundLightHex = toHex(backgroundLightFinal[0], backgroundLightFinal[1], backgroundLightFinal[2]);
	cout << backgroundDarkHex << endl;
	replaceFile("Default-3/_colors.scss", MonetObjects::GTK3::Light::monetAccent, hexFinal);
	replaceFile("Default-4/_colors.scss", MonetObjects::GTK4::Light::monetAccent, hexFinal);
	replaceFile("Gnome-4/gnome-shell-sass/_colors.scss", MonetObjects::GNOME::Light::monetAccent, hexFinal);
	replaceFile("Default-3/_colors.scss", MonetObjects::GTK3::Dark::baseColor, backgroundDarkHex);
	replaceFile("Default-3/_colors.scss", MonetObjects::GTK3::Light::baseColor, backgroundLightHex);
	replaceFile("Default-4/_colors.scss", MonetObjects::GTK4::Dark::baseColor, backgroundDarkHex);
	replaceFile("Default-4/_colors.scss", MonetObjects::GTK4::Light::baseColor, backgroundLightHex);

	replaceFile("Default-3/_colors.scss", MonetObjects::GTK3::Dark::monetLightColor, backgroundLightHex);
	replaceFile("Default-3/_colors.scss", MonetObjects::GTK3::Light::monetDarkColor, backgroundDarkHex);
	replaceFile("Default-4/_colors.scss", MonetObjects::GTK4::Dark::monetLightColor, backgroundLightHex);
	replaceFile("Default-4/_colors.scss", MonetObjects::GTK4::Light::monetDarkColor, backgroundDarkHex);

	replaceFile("Gnome-4/gnome-shell-sass/_colors.scss", MonetObjects::GNOME::Dark::baseColor, backgroundDarkHex);
	replaceFile("Gnome-4/gnome-shell-sass/_colors.scss", MonetObjects::GNOME::Light::baseColor, backgroundLightHex);
	replaceFile("Gnome-4/gnome-shell-sass/_colors.scss", MonetObjects::GNOME::Dark::monetBaseColor, backgroundDarkHex);
	replaceFile("Gnome-4/gnome-shell-sass/_colors.scss", MonetObjects::GNOME::Light::monetBaseColor, backgroundDarkHex);
	compile();
	return 0;
}
